/**
 *
 * GC1
 * Game Console 1
 *
 * @author VisionMise <visionmise@psyn.app>
 * @copyright 2022 Geoffrey Kuhl
 * @version 0.3.0
 */
var Game;
(function (Game) {
    let power;
    (function (power) {
        power[power["off"] = 0] = "off";
        power[power["on"] = 1] = "on";
    })(power = Game.power || (Game.power = {}));
    class Console {
        //Console State
        state = power.off;
        //Graphics Rendering
        renderer;
        //SPU Clock and Timing
        clock;
        //Intanciate Console
        constructor() {
            //Add SPU Clock and Graphics
            this.clock = new Clock(this);
            this.renderer = new Graphics();
            this.turnOn();
        }
        get spu() {
            return this.clock;
        }
        get graphics() {
            return this.renderer;
        }
        get power() {
            return this.state;
        }
        set power(on) {
            this.state = on;
            this.clock.running = Boolean(this.power);
        }
        reset() {
            document.location.reload();
        }
        turnOn() {
            this.power = power.on;
        }
        turnOff() {
            this.power = power.off;
        }
        bootstrap() {
        }
    }
    Game.Console = Console;
    class Clock {
        lastTimestamp = 0;
        deltaTime = 0;
        state = false;
        clockLoop;
        console;
        constructor(gameConsole) {
            this.console = gameConsole;
            this.clockLoop = new CustomEvent('clock_loop', { detail: this });
        }
        get running() {
            return this.state;
        }
        set running(running) {
            this.state = running;
            console.log(this.state, this.state == true);
            if (this.state == true)
                this.run(0);
        }
        get time() {
            return this.deltaTime;
        }
        run(timestamp) {
            console.log("SA", this.running);
            this.deltaTime = timestamp - this.lastTimestamp;
            this.lastTimestamp = timestamp;
            if (this.running)
                requestAnimationFrame(this.run);
            dispatchEvent(this.clockLoop);
        }
    }
    class Graphics {
        framePerSecond = 60;
        frameIteration = 0;
        animationLoop;
        constructor() {
            this.animationLoop = new CustomEvent('animation_loop', { detail: this });
            window.addEventListener('clock_loop', (event) => this.render(event.detail));
        }
        get fps() {
            return this.framePerSecond;
        }
        set fps(fps) {
            this.framePerSecond = fps;
        }
        get iteration() {
            return this.frameIteration;
        }
        render(clock) {
            //dont let the clock time exceed the
            //fps - will cause infinate loop
            let increment = (clock.time >= this.fps) ? this.fps - 1 : clock.time;
            this.frameIteration += increment;
            if (this.frameIteration >= this.framePerSecond) {
                let remainder = this.frameIteration - this.framePerSecond;
                this.frameIteration = remainder;
                dispatchEvent(this.animationLoop);
            }
        }
    }
    class Loader {
        console;
        gameLoaded = false;
        constructor(gameConsole) {
            this.console = gameConsole;
        }
        get loaded() {
            return this.gameLoaded;
        }
    }
})(Game || (Game = {}));
const gc1 = new Game.Console();
// window['gc1'] = gc1;
window.addEventListener('animation_loop', (e) => {
    console.log(e?.detail);
});
gc1.turnOn();
console.log(gc1);
