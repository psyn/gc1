/**
 * 
 * GC1 
 * Game Console 1
 * 
 * @author VisionMise <visionmise@psyn.app>
 * @copyright 2022 Geoffrey Kuhl
 * @version 0.3.0
 */
namespace Game {

    export enum power {
        off = 0,
        on  = 1
    }

    export class Console {

        //Console State
        private state:power = power.off;

        //Graphics Rendering
        private renderer:Graphics;

        //SPU Clock and Timing
        private clock:Clock;


        //Intanciate Console
        constructor() {

            //Add SPU Clock and Graphics
            this.clock      = new Clock(this);
            this.renderer   = new Graphics();

            this.turnOn();

        }

        public get spu() : Clock {
            return this.clock;
        }

        public get graphics() : Graphics {
            return this.renderer;
        }

        public get power() : power {
            return this.state;
        }

        public set power(on:power) {
            this.state          = on;
            this.clock.running  = Boolean(this.power);
        }

        public reset() : void {
            document.location.reload();
        }

        public turnOn() : void {
            this.power = power.on;
        }

        public turnOff() : void {
            this.power = power.off;
        }

        private bootstrap() : void {
            
        }

    }

    class Clock {

        private lastTimestamp:number        = 0;
        private deltaTime:number            = 0;
        private state:boolean               = false;
        private clockLoop:CustomEvent;
        private console:Console;

        constructor(gameConsole:Game.Console) {
            this.console    = gameConsole;
            this.clockLoop  = new CustomEvent('clock_loop', {detail: this});
        }

        public get running() : boolean {
            return this.state;
        }

        public set running(running:boolean) {
            this.state = running;
            console.log(this.state, this.state == true)

            if (this.state == true) this.run(0);
        }

        public get time() : number {
            return this.deltaTime;
        }

        public run(timestamp:number) : void {
            console.log("SA", this.running);
            this.deltaTime      = timestamp - this.lastTimestamp;
            this.lastTimestamp  = timestamp;
            if (this.running) requestAnimationFrame(this.run);
            dispatchEvent(this.clockLoop);
        }

    }

    class Graphics {

        private framePerSecond:number = 60;
        private frameIteration:number = 0;
        private animationLoop:CustomEvent;

        public constructor() {
            this.animationLoop = new CustomEvent('animation_loop', {detail: this});
            window.addEventListener('clock_loop', (event:CustomEvent) => this.render(event.detail));
        }

        public get fps() : number {
            return this.framePerSecond;
        }

        public set fps(fps:number) {
            this.framePerSecond = fps;
        }

        public get iteration() : number {
            return this.frameIteration;
        }

        public render(clock:Clock) {

            //dont let the clock time exceed the
            //fps - will cause infinate loop
            let increment:number = (clock.time >= this.fps) ? this.fps - 1: clock.time;

            this.frameIteration += increment;

            if (this.frameIteration >= this.framePerSecond) {
                let remainder:number    = this.frameIteration - this.framePerSecond;
                this.frameIteration     = remainder;

                dispatchEvent(this.animationLoop);
            }
            
        }
    }

    class Interpreter {
        public constructor() {

        }
    }

    class Loader {

        private console:Game.Console;
        private gameLoaded:boolean = false;

        public constructor(gameConsole:Game.Console) {
            this.console = gameConsole;
        }

        public get loaded() : boolean {
            return this.gameLoaded;
        }
    }

}

const gc1:Game.Console = new Game.Console();

window.addEventListener('animation_loop', (e:CustomEvent) => {
    console.log(e?.detail);
});

gc1.turnOn()

console.log(gc1);